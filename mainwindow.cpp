#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <iostream>
#include <QDir>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    QPixmap pixmap("J:/MP3_Demo/play.png");
    QIcon ButtonIcon(pixmap);
    ui->playBtn->setIcon(ButtonIcon);
    ui->playBtn->setIconSize(pixmap.rect().size());
    pixmap = QPixmap("J:/MP3_Demo/pause.png");
    ButtonIcon = QIcon(pixmap);
    ui->pauseBtn->setIcon(ButtonIcon);
    ui->pauseBtn->setIconSize(pixmap.rect().size());
    pixmap = QPixmap("J:/MP3_Demo/next.png");
    ButtonIcon = QIcon(pixmap);
    ui->nextBtn->setIcon(ButtonIcon);
    ui->nextBtn->setIconSize(pixmap.rect().size());
    pixmap = QPixmap("J:/MP3_Demo/prev.png");
    ButtonIcon = QIcon(pixmap);
    ui->prevBtn->setIcon(ButtonIcon);
    ui->prevBtn->setIconSize(pixmap.rect().size());
    pixmap = QPixmap("J:/MP3_Demo/playpause.png");
    ButtonIcon = QIcon(pixmap);
    ui->playPauseBtn->setIcon(ButtonIcon);
    ui->playPauseBtn->setIconSize(pixmap.rect().size());

    QStringList filter;
    filter << "*.mp3";
    QString dir("/");
    files = new QFileSystemModel();
    files->setNameFilters(filter);
    files->setRootPath(dir);
    ui->treeView->setModel(files);

    //ui->art->setScene(scene);

    updater = new QTimer(this);
    connect(updater, SIGNAL(timeout()), this, SLOT(update()));
    updater->start(100);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::update()
{
    //qDebug() << "boi" << '\n';
    if (mediaPlayer.mediaStatus() != QMediaPlayer::NoMedia && mediaPlayer.duration() > 0)
    {
        qint64 pos  = (mediaPlayer.position() * 1000) / mediaPlayer.duration();
        //qDebug() << "pos" << mediaPlayer.position() << "dur" << mediaPlayer.duration() << '\n';
        ui->horizontalSlider->setValue(pos);
        ui->title->setText(mediaPlayer.metaData("Title").toString());
        ui->album->setText(mediaPlayer.metaData("AlbumTitle").toString());
        ui->artist->setText(mediaPlayer.metaData("ContributingArtist").toString());
        //imageItem = new QGraphicsPixmapItem(mediaPlayer.metaData("CoverArtImage").value<QPixmap>());
        //scene->addItem(imageItem);
    }
}

void MainWindow::on_fileBtn_clicked()
{
    QString dir = QFileDialog::getExistingDirectory(this, tr("Open Directory"),"/");

    if (!dir.isEmpty()) {
            const QModelIndex rootIndex = files->index(QDir::cleanPath(dir));
            if (rootIndex.isValid())
                ui->treeView->setRootIndex(rootIndex);
    }

    ui->lineEdit->setText(dir);
}

void MainWindow::on_playBtn_clicked()
{
    mediaPlayer.play();
    playing = true;
}

void MainWindow::on_pauseBtn_clicked()
{
    mediaPlayer.pause();
    playing = false;
}

void MainWindow::on_treeView_doubleClicked(const QModelIndex &selected)
{
    //QModelIndex selected = ui->treeView->selectionModel()->selectedIndexes()[0];
    QFileInfo fileInfo = files->fileInfo(selected);
    qDebug() << fileInfo.absoluteFilePath() << '\n';
    mediaPlayer.setMedia(QUrl::fromLocalFile(fileInfo.absoluteFilePath()));
    mediaPlayer.play();
    playing = true;
}

void MainWindow::on_playPauseBtn_clicked()
{
    if (playing){
        playing = false;
        mediaPlayer.pause();
    }
    else {
        playing = true;
        mediaPlayer.play();
    }
}
