#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QtWidgets>
#include <QtWinExtras>
#include <QMediaPlayer>
#include <QDir>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

    void play_default_song();

private slots:
    void on_playBtn_clicked();
    void update();
    void on_fileBtn_clicked();

    void on_pauseBtn_clicked();

    void on_treeView_doubleClicked(const QModelIndex &index);

    void on_playPauseBtn_clicked();

private:
    QTimer * updater;
    QGraphicsScene * scene;
    //QGraphicsPixmapItem * imageItem;
    Ui::MainWindow *ui;
    QDir musicPath;
    QFileSystemModel * files;
    QMediaPlayer mediaPlayer;
    bool playing = false;
};

#endif // MAINWINDOW_H
