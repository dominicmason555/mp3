#-------------------------------------------------
#
# Project created by QtCreator 2017-12-17T18:17:38
#
#-------------------------------------------------

QT       += core gui
QT += widgets multimedia winextras

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = MP3_Demo
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp

HEADERS  += mainwindow.h

FORMS    += mainwindow.ui
